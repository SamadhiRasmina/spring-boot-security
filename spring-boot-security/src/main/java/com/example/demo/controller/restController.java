package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class restController {
    @GetMapping("/get")
    public String home(){
        return ("<h1>Welcome</h1>");
    }
}
